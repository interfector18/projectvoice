#include "Utils.hpp"

namespace fs = std::filesystem;

// std::vector<double> calculateAvgMfcc(const fs::path &p, phoneme ph)
// {
//     auto mfccs = calculateMfcc(p, ph);
//     auto mfcc = mfccs[0];
//     for (int i = 1; i < mfccs.size(); ++i)
//     {
//         mfcc += mfccs[i];
//     }

//     return mfcc / mfccs.size();
// }

std::vector<double> calculateAvgMfcc(const fs::path &p, phoneme ph)
{
    constexpr double alpha = 0.97;
    constexpr double epsilon = 1.0;
    constexpr int mfccNum = 14; // 14 will calculate 13 coeficients, + c0 and energy -> 15 total length of array;

    const int phLength = ph.end - ph.start;

    SndfileHandle sf(p.string());
    sf.seek(ph.start, 0);

    std::vector<double> signal(phLength * 2);
    std::vector<std::vector<double>> mfccs;
    std::vector<double> mfcc(mfccNum + 1, 0);

    sf.read(signal.data(), phLength);

    auto frameLength = highestPowerof2(phLength);

    sptk::mfcc(signal.data(),
               mfcc.data(),
               sf.samplerate() / 1000,
               alpha,
               epsilon,
               frameLength,
               frameLength,
               mfccNum,
               20,
               22,
               sptk::Boolean::TR,
               sptk::Boolean::TR);

    return mfcc;
}

std::vector<std::vector<double>> calculateMfcc(const fs::path &p, phoneme ph)
{
    constexpr double alpha = 0.97;
    constexpr double epsilon = 1.0;
    constexpr int mfccNum = 14; // 14 will calculate 13 coeficients, + c0 and energy -> 15 total length of array;

    const int phLength = ph.end - ph.start;

    SndfileHandle sf(p.string());
    sf.seek(ph.start, 0);

    std::vector<double> signal(phLength * 2);
    std::vector<std::vector<double>> mfccs;
    std::vector<double> mfcc(5000, 0);

    sf.read(signal.data(), phLength);

    auto frameLength = 32;
    auto frameSliding = 16;
    auto insertPoint = 0;

    for (int frameStart = 0; frameStart + frameLength < phLength; frameStart += frameSliding)
    {
        sptk::mfcc(signal.data() + frameStart,
                   mfcc.data() + insertPoint,
                   sf.samplerate() / 1000,
                   alpha,
                   epsilon,
                   frameLength,
                   frameLength,
                   mfccNum,
                   20,
                   22,
                   sptk::Boolean::TR,
                   sptk::Boolean::TR);
        insertPoint += mfccNum + 1;
        // if (auto error = std::accumulate(mfcc.begin() + mfccNum + 1, mfcc.end(), 0);
        //     error != 0)
        // {
        //     std::cout << "\n\n\n\n\n ERRRRRROOOOORRRR \n " << error << "\n\n\n\n\n";
        // }
        // std::cout << std::flush;
    }
    for (int i = 0; i < insertPoint; i += mfccNum + 1)
        mfccs.emplace_back(mfcc.begin() + i, mfcc.begin() + i + mfccNum + 1);
    // std::cout << mfccs[mfccs.size() - 1] <<;

    return mfccs;
}

int highestPowerof2(int n)
{
    int res = 0;
    for (int i = n; i >= 1; i--)
    {
        // If i is a power of 2
        if ((i & (i - 1)) == 0)
        {
            res = i;
            break;
        }
    }
    return res;
}