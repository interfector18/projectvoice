#include <cstddef>
#include <cstdio>
namespace sptk
{
extern "C"
{
#include <SPTK.h>
}
} // namespace sptk

#include "Dataset.hpp"
#include "Models.hpp"
#include <algorithm>
#include <filesystem>
#include <fmt/format.h>
#include <iostream>
#include <sndfile.hh>
#include "Utils.hpp"



int main(int argc, char const *argv[])
{

    auto path = "/home/deus/VoiceDataset/data/lisa/data/timit/raw/TIMIT/";

    auto [wholeSet, phonemeToId] = getDataset(path);

    int min = 5001;
    int max = 0;
    phoneme mm, mi;

    for (const auto &phoneme : wholeSet)
    {
        calculateMfcc(path + phoneme.relPath, phoneme);
        int length = phoneme.end - phoneme.start;
        if (length > max)
        {
            max = length;
            mm = phoneme;
        }
        if (length < min)
        {
            min = length;
            mi = phoneme;
        }
        // std::cout << fmt::format("Phoneme {}, length {} \n", phoneme.phoneme, phoneme.end - phoneme.start);
    }
    std::cout << fmt::format("Max: {} -> phoneme {}, Min: {} -> phoneme {}\n", max, mm.phoneme, min, mi.phoneme);

    return 0;
}



