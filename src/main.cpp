#include <cstddef>
#include <cstdio>
namespace sptk
{
extern "C"
{
#include <SPTK.h>
}
} // namespace sptk

#include "Dataset.hpp"
#include "Models.hpp"
#include <fmt/format.h>
#include <iostream>
#include <torch/torch.h>

struct Options
{
    double learning_rate = 0.001;
    size_t batch_size = 1000;
    size_t epochs = 20;
    torch::DeviceType device = torch::kCPU;
};

static Options options;

struct NetworkImpl : torch::nn::Module
{
    NetworkImpl()
    {
        using namespace torch::nn;

        fc1 = register_module("fc1", Linear(5000, 2500));
        fc2 = register_module("fc2", Linear(2500, 2500));
        fc3 = register_module("fc3", Linear(2500, 2500));
        fc7 = register_module("fc7", Linear(2500, 1250));
        fc8 = register_module("fc8", Linear(1250, 200));
        fc9 = register_module("fc9", Linear(200, 61));
    }

    torch::Tensor forward(torch::Tensor x)
    {
        // x = torch::dropout(x, /*p=*/0.5, /*train=*/is_training());
        x = torch::selu(fc1->forward(x));
        x = torch::selu(fc2->forward(x));
        x = torch::selu(fc3->forward(x));
        x = torch::selu(fc7->forward(x));
        x = torch::selu(fc8->forward(x));
        x = torch::selu(fc9->forward(x)); //, /*dim=*/1;
        // x = torch::sigmoid(x);
        return x;
    }

    torch::nn::Linear fc1{nullptr};
    torch::nn::Linear fc2{nullptr};
    torch::nn::Linear fc3{nullptr};
    torch::nn::Linear fc7{nullptr};
    torch::nn::Linear fc8{nullptr};
    torch::nn::Linear fc9{nullptr};
};
TORCH_MODULE(Network);

template <typename DataLoader, typename Model, typename Criterion>
void train(
    Model &model,
    DataLoader &loader,
    torch::optim::Optimizer &optimizer,
    Criterion criterion,
    size_t epoch,
    size_t data_size)
{
    size_t index = 0;
    model->train();
    float Loss = 0, Acc = 0;

    for (auto &batch : loader)
    {
        auto data = batch.data.unsqueeze(1).to(options.device);
        // auto data = batch.data.to(options.device);
        auto targets = batch.target.to(options.device);

        auto output = model(data);

        auto loss = criterion(output, targets);
        assert(!std::isnan(loss.template item<float>()));
        auto acc = output.argmax(1).eq(targets).sum();

        optimizer.zero_grad();
        loss.backward();
        optimizer.step();

        Loss += loss.template item<float>();
        Acc += acc.template item<float>();

        auto end = std::min(data_size, (index++ + 1) * options.batch_size);

        std::cout << fmt::format("Train Epoch: {}, {} / {}, Loss: {:e}, Acc: {:2.4e}\r",
                                 epoch, end, data_size, Loss / end, Acc / end);
    }
}

template <typename DataLoader, typename Model, typename Criterion>
void test(Model &model, DataLoader &loader, Criterion criterion, size_t data_size)
{
    model->eval();
    torch::NoGradGuard no_grad;
    float Loss = 0, Acc = 0;

    for (const auto &batch : loader)
    {
        auto data = batch.data.unsqueeze(1).to(options.device);
        // auto data = batch.data.to(options.device);
        auto targets = batch.target.to(options.device);

        auto output = model(data);

        auto loss = criterion(output, targets);
        auto acc = output.argmax(1).eq(targets).sum();

        Loss += loss.template item<float>();
        Acc += acc.template item<float>();
    }

    std::cout << fmt::format("Test Loss: {:e}, Acc: {:e}\r", Loss / data_size, Acc / data_size);
}

int main(int argc, char const *argv[])
{
    torch::manual_seed(1);

    if (torch::cuda::is_available())
        options.device = torch::kCUDA;
    std::cout << "Running on: "
              << (options.device == torch::kCUDA ? "CUDA" : "CPU")
              << "\n";

    // sptk::mfcc((double *)arr, (double *)stuff, 3, 1, 1, 1, 2, 2, 1, 2, sptk::Boolean::FA, sptk::Boolean::FA);

    auto path = "/home/deus/VoiceDataset/data/lisa/data/timit/raw/TIMIT/";

    auto [wholeSet, phonemeToId] = getDataset(path);
    std::random_shuffle(wholeSet.begin(), wholeSet.end());
    auto [trainData, testData] = split_dataset(wholeSet, 30.0);
    auto trainSet = ProjectVoiceDataset(trainData, phonemeToId, path);
    auto testSet = ProjectVoiceDataset(testData, phonemeToId, path);

    std::cout << std::boolalpha;

    std::cout << *trainSet.size() << "\n";
    std::cout << *testSet.size() << "\n";

    auto data_loader_options = torch::data::DataLoaderOptions()
                                   .batch_size(options.batch_size)
                                   .workers(1);

    auto train_set =
        trainSet.map(torch::data::transforms::Stack<>());
    auto train_size = train_set.size().value();
    auto train_loader =
        torch::data::make_data_loader<torch::data::samplers::SequentialSampler>(
            std::move(train_set), data_loader_options);

    auto test_set =
        testSet.map(torch::data::transforms::Stack<>());
    auto test_size = test_set.size().value();
    auto test_loader =
        torch::data::make_data_loader<torch::data::samplers::SequentialSampler>(
            std::move(test_set), data_loader_options);

    ProjectVoice::ResNet18_1d network;
    // Network network;
    network->to(options.device);

    auto criterion = torch::nn::CrossEntropyLoss();
    torch::optim::SGD optimizer(
        network->parameters(), torch::optim::SGDOptions(options.learning_rate).momentum(0.5));

    for (size_t i = 0; i < options.epochs; ++i)
    {
        train(network, *train_loader, optimizer, criterion, i + 1, train_size);
        std::cout << "\n";
        test(network, *test_loader, criterion, test_size);
        std::cout << "\n";
    }

    return 0;
}
