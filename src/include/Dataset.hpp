#ifndef DATASET_HPP
#define DATASET_HPP
#include <array>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <map>
#include <string>
#include <torch/torch.h>
#include <tuple>
#include <unordered_map>
#include <vector>

struct phoneme
{
    std::string relPath;
    int start = 0;
    int end = 0;
    std::string phoneme;
    long id;
};

template <typename T>
auto split_dataset(const std::vector<T> &whole_set, const double &percetage_for_test)
{
    using Data = std::vector<T>;

    size_t total_size = whole_set.size();
    size_t test_size = round(total_size * percetage_for_test / 100.0);

    size_t train_dev_border = total_size - test_size;

    return std::tuple<Data, Data>(Data(whole_set.begin(), whole_set.begin() + train_dev_border), Data(whole_set.begin() + train_dev_border, whole_set.end()));
}

template <typename T>
auto split_dataset(const std::vector<T> &&whole_set, const double &percetage_for_test)
{
    using Data = std::vector<T>;

    size_t total_size = whole_set.size();
    size_t test_size = round(total_size * percetage_for_test / 100.0);

    size_t train_dev_border = total_size - test_size;

    return std::tuple<Data, Data>(Data(whole_set.begin(), whole_set.begin() + train_dev_border), Data(whole_set.begin() + train_dev_border, whole_set.end()));
}

template <typename T>
auto split_dataset(const std::vector<T> &whole_set, const std::array<double, 2> &percetages_for_dev_and_test)
{
    using Data = std::vector<T>;

    size_t total_size = whole_set.size();
    size_t dev_size = round(total_size * percetages_for_dev_and_test[0] / 100.0);
    size_t test_size = round(total_size * percetages_for_dev_and_test[1] / 100.0);

    size_t train_dev_border = total_size - (dev_size + test_size);
    size_t dev_test_border = total_size - (dev_size);

    return std::tuple(Data(whole_set.begin(), whole_set.begin() + train_dev_border),
                      Data(whole_set.begin() + train_dev_border, whole_set.begin() + dev_test_border),
                      Data(whole_set.begin() + dev_test_border, whole_set.end()));
}

template <typename T>
auto split_dataset(const std::vector<T> &&whole_set, const std::array<double, 2> &percetages_for_dev_and_test)
{
    using Data = std::vector<T>;

    size_t total_size = whole_set.size();
    size_t dev_size = round(total_size * percetages_for_dev_and_test[0] / 100.0);
    size_t test_size = round(total_size * percetages_for_dev_and_test[1] / 100.0);

    size_t train_dev_border = total_size - (dev_size + test_size);
    size_t dev_test_border = total_size - (dev_size);

    return std::tuple(Data(whole_set.begin(), whole_set.begin() + train_dev_border),
                      Data(whole_set.begin() + train_dev_border, whole_set.begin() + dev_test_border),
                      Data(whole_set.begin() + dev_test_border, whole_set.end()));
}

namespace fs = std::filesystem;

std::tuple<std::vector<phoneme>, std::unordered_map<std::string, long>> getDataset(const fs::path &p);

struct ProjectVoiceDataset : public torch::data::datasets::Dataset<ProjectVoiceDataset>
{

    using Example = torch::data::Example<>;
    fs::path basePath;
    std::vector<phoneme> data;
    torch::Tensor toOneHot(long id);

public:
    std::unordered_map<std::string, long> phonemeToId;
    ProjectVoiceDataset(const std::vector<phoneme> &d,
                        const std::unordered_map<std::string, long> &idMap,
                        const fs::path &p);

    Example get(size_t index);
    torch::optional<size_t> size() const;
};

#endif // !DATASET_HPP
