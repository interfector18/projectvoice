#ifndef UTILS_HPP
#define UTILS_HPP

#include <cstddef>
#include <cstdio>
namespace sptk
{
extern "C"
{
#include <SPTK.h>
}
} // namespace sptk

#include "Dataset.hpp"
#include <algorithm>
#include <filesystem>
#include <fmt/format.h>
#include <iostream>
#include <sndfile.hh>

namespace fs = std::filesystem;

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v)
{
    for (const auto &e : v)
        os << e;
    return os;
}

template <typename T>
std::vector<T> operator/(const std::vector<T> &a, int divisor)
{
    std::vector<T> res(a.size(), T{});
    for (int i = 0; i < a.size(); ++i)
    {
        res[i] = a[i] / divisor;
    }
    return res;
}

template <typename T>
std::vector<T> operator+(const std::vector<T> &a, const std::vector<T> &b)
{
    std::vector<T> res(a.size(), T{});
    for (int i = 0; i < a.size(); ++i)
    {
        res[i] = a[i] + b[i];
    }
    return res;
}

template <typename T>
std::vector<T> &operator+=(const std::vector<T> &a, const std::vector<T> &b)
{
    std::vector<T> res(a.size(), T{});
    // res.reserve(a.size());
    // for (int i = 0; i < a.size(); ++i)
    // {
    //     // res[i] =
    //     a[i] += b[i];
    // }

    std::transform(a.begin(), a.end(), b.begin(), res.begin(), std::plus<T>());

    return res;
}

std::vector<double> calculateAvgMfcc(const fs::path &p, phoneme ph);
std::vector<std::vector<double>> calculateMfcc(const fs::path &p, phoneme ph);
int highestPowerof2(int n);

#endif // !UTILS_HPP