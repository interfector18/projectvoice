#ifndef RESNET_HPP
#define RESNET_HPP

#include <torch/torch.h>

namespace ProjectVoice
{
template <typename Block>
struct ResNet_2dImpl;

namespace _resnet_2dimpl
{
// 3x3 convolution with padding
torch::nn::Conv2d conv3x3_2d(
    int64_t in,
    int64_t out,
    int64_t stride = 1,
    int64_t groups = 1);

// 1x1 convolution
torch::nn::Conv2d conv1x1_2d(int64_t in, int64_t out, int64_t stride = 1);

struct BasicBlock : torch::nn::Module
{
    template <typename Block>
    friend struct ProjectVoice::ResNet_2dImpl;

    int64_t stride;
    torch::nn::Sequential downsample;

    torch::nn::Conv2d conv1{nullptr}, conv2{nullptr};
    torch::nn::BatchNorm2d bn1{nullptr}, bn2{nullptr};

    static int expansion;

    BasicBlock(
        int64_t inplanes,
        int64_t planes,
        int64_t stride = 1,
        torch::nn::Sequential downsample = nullptr,
        int64_t groups = 1,
        int64_t base_width = 64);

    torch::Tensor forward(torch::Tensor x);
};

struct Bottleneck : torch::nn::Module
{
    template <typename Block>
    friend struct ProjectVoice::ResNet_2dImpl;

    int64_t stride;
    torch::nn::Sequential downsample;

    torch::nn::Conv2d conv1{nullptr}, conv2{nullptr}, conv3{nullptr};
    torch::nn::BatchNorm2d bn1{nullptr}, bn2{nullptr}, bn3{nullptr};

    static int expansion;

    Bottleneck(
        int64_t inplanes,
        int64_t planes,
        int64_t stride = 1,
        torch::nn::Sequential downsample = nullptr,
        int64_t groups = 1,
        int64_t base_width = 64);

    torch::Tensor forward(torch::Tensor X);
};
} // namespace _resnet_2dimpl

template <typename Block>
struct ResNet_2dImpl : torch::nn::Module
{
    int64_t groups, base_width, inplanes;
    torch::nn::Conv2d conv1;
    torch::nn::BatchNorm2d bn1;
    torch::nn::Sequential layer1, layer2, layer3, layer4;
    torch::nn::Linear fc;

    torch::nn::Sequential _make_layer(
        int64_t planes,
        int64_t blocks,
        int64_t stride = 1);

    ResNet_2dImpl(
        const std::vector<int> &layers,
        int64_t num_classes = 1000,
        bool zero_init_residual = false,
        int64_t groups = 1,
        int64_t width_per_group = 64);

    torch::Tensor forward(torch::Tensor X);
};

template <typename Block>
torch::nn::Sequential ResNet_2dImpl<Block>::_make_layer(
    int64_t planes,
    int64_t blocks,
    int64_t stride)
{
    torch::nn::Sequential downsample = nullptr;
    if (stride != 1 || inplanes != planes * Block::expansion)
    {
        downsample = torch::nn::Sequential(
            _resnet_2dimpl::conv1x1_2d(inplanes, planes * Block::expansion, stride),
            torch::nn::BatchNorm2d(planes * Block::expansion));
    }

    torch::nn::Sequential layers;
    layers->push_back(
        Block(inplanes, planes, stride, downsample, groups, base_width));

    inplanes = planes * Block::expansion;

    for (int i = 1; i < blocks; ++i)
        layers->push_back(Block(inplanes, planes, 1, nullptr, groups, base_width));

    return layers;
}

template <typename Block>
ResNet_2dImpl<Block>::ResNet_2dImpl(
    const std::vector<int> &layers,
    int64_t num_classes,
    bool zero_init_residual,
    int64_t groups,
    int64_t width_per_group)
    : groups(groups),
      base_width(width_per_group),
      inplanes(64),
      conv1(
          torch::nn::Conv2dOptions(3, 64, 7).stride(2).padding(3).bias(false)),
      bn1(64),
      layer1(_make_layer(64, layers[0])),
      layer2(_make_layer(128, layers[1], 2)),
      layer3(_make_layer(256, layers[2], 2)),
      layer4(_make_layer(512, layers[3], 2)),
      fc(512 * Block::expansion, num_classes)
{
    register_module("conv1", conv1);
    register_module("bn1", bn1);
    register_module("fc", fc);

    register_module("layer1", layer1);
    register_module("layer2", layer2);
    register_module("layer3", layer3);
    register_module("layer4", layer4);

    for (auto &module : modules(/*include_self=*/false))
    {
        if (auto M = dynamic_cast<torch::nn::Conv2dImpl *>(module.get()))
            torch::nn::init::kaiming_normal_(
                M->weight,
                /*a=*/0,
                torch::kFanOut,
                torch::kReLU);
        else if (auto M = dynamic_cast<torch::nn::BatchNormImpl *>(module.get()))
        {
            torch::nn::init::constant_(M->weight, 1);
            torch::nn::init::constant_(M->bias, 0);
        }
    }

    // Zero-initialize the last BN in each residual branch, so that the residual
    // branch starts with zeros, and each residual block behaves like an
    // identity. This improves the model by 0.2~0.3% according to
    // https://arxiv.org/abs/1706.02677
    if (zero_init_residual)
        for (auto &module : modules(/*include_self=*/false))
        {
            if (auto *M = dynamic_cast<_resnet_2dimpl::Bottleneck *>(module.get()))
                torch::nn::init::constant_(M->bn3->weight, 0);
            else if (auto *M = dynamic_cast<_resnet_2dimpl::BasicBlock *>(module.get()))
                torch::nn::init::constant_(M->bn2->weight, 0);
        }
}

template <typename Block>
torch::Tensor ResNet_2dImpl<Block>::forward(torch::Tensor x)
{
    x = conv1->forward(x);
    x = bn1->forward(x).relu_();
    x = torch::max_pool2d(x, 3, 2, 1);

    x = layer1->forward(x);
    x = layer2->forward(x);
    x = layer3->forward(x);
    x = layer4->forward(x);

    x = torch::adaptive_avg_pool2d(x, {1, 1});
    x = x.reshape({x.size(0), -1});
    x = fc->forward(x);

    return x;
}

struct ResNet18_2dImpl : ResNet_2dImpl<_resnet_2dimpl::BasicBlock>
{
    ResNet18_2dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet34_2dImpl : ResNet_2dImpl<_resnet_2dimpl::BasicBlock>
{
    ResNet34_2dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet50_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    ResNet50_2dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet101_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    ResNet101_2dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet152_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    ResNet152_2dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNext50_32x4d_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    ResNext50_32x4d_2dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct ResNext101_32x8d_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    ResNext101_32x8d_2dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct WideResNet50_2_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    WideResNet50_2_2dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct WideResNet101_2_2dImpl : ResNet_2dImpl<_resnet_2dimpl::Bottleneck>
{
    WideResNet101_2_2dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

template <typename Block>
struct ResNet_2d : torch::nn::ModuleHolder<ResNet_2dImpl<Block>>
{
    using torch::nn::ModuleHolder<ResNet_2dImpl<Block>>::ModuleHolder;
};

template <typename Block>
struct ResNet_1dImpl;

namespace _resnet_1dimpl
{
// 3x3 convolution with padding
torch::nn::Conv1d conv3x3_1d(
    int64_t in,
    int64_t out,
    int64_t stride = 1,
    int64_t groups = 1);

// 1x1 convolution
torch::nn::Conv1d conv1x1_1d(int64_t in, int64_t out, int64_t stride = 1);

struct BasicBlock : torch::nn::Module
{
    template <typename Block>
    friend struct ProjectVoice::ResNet_1dImpl;

    int64_t stride;
    torch::nn::Sequential downsample;

    torch::nn::Conv1d conv1{nullptr}, conv2{nullptr};
    torch::nn::BatchNorm1d bn1{nullptr}, bn2{nullptr};

    static int expansion;

    BasicBlock(
        int64_t inplanes,
        int64_t planes,
        int64_t stride = 1,
        torch::nn::Sequential downsample = nullptr,
        int64_t groups = 1,
        int64_t base_width = 64);

    torch::Tensor forward(torch::Tensor x);
};

struct Bottleneck : torch::nn::Module
{
    template <typename Block>
    friend struct ProjectVoice::ResNet_1dImpl;

    int64_t stride;
    torch::nn::Sequential downsample;

    torch::nn::Conv1d conv1{nullptr}, conv2{nullptr}, conv3{nullptr};
    torch::nn::BatchNorm1d bn1{nullptr}, bn2{nullptr}, bn3{nullptr};

    static int expansion;

    Bottleneck(
        int64_t inplanes,
        int64_t planes,
        int64_t stride = 1,
        torch::nn::Sequential downsample = nullptr,
        int64_t groups = 1,
        int64_t base_width = 64);

    torch::Tensor forward(torch::Tensor X);
};
} // namespace _resnet_1dimpl

template <typename Block>
struct ResNet_1dImpl : torch::nn::Module
{
    int64_t groups, base_width, inplanes;
    torch::nn::Conv1d conv1;
    torch::nn::BatchNorm1d bn1;
    torch::nn::Sequential layer1, layer2, layer3, layer4;
    torch::nn::Linear fc;

    torch::nn::Sequential _make_layer(
        int64_t planes,
        int64_t blocks,
        int64_t stride = 1);

    ResNet_1dImpl(
        const std::vector<int> &layers,
        int64_t num_classes = 1000,
        bool zero_init_residual = false,
        int64_t groups = 1,
        int64_t width_per_group = 64);

    torch::Tensor forward(torch::Tensor X);
};

template <typename Block>
torch::nn::Sequential ResNet_1dImpl<Block>::_make_layer(
    int64_t planes,
    int64_t blocks,
    int64_t stride)
{
    torch::nn::Sequential downsample = nullptr;
    if (stride != 1 || inplanes != planes * Block::expansion)
    {
        downsample = torch::nn::Sequential(
            _resnet_1dimpl::conv1x1_1d(inplanes, planes * Block::expansion, stride),
            torch::nn::BatchNorm1d(planes * Block::expansion));
    }

    torch::nn::Sequential layers;
    layers->push_back(
        Block(inplanes, planes, stride, downsample, groups, base_width));

    inplanes = planes * Block::expansion;

    for (int i = 1; i < blocks; ++i)
        layers->push_back(Block(inplanes, planes, 1, nullptr, groups, base_width));

    return layers;
}

template <typename Block>
ResNet_1dImpl<Block>::ResNet_1dImpl(
    const std::vector<int> &layers,
    int64_t num_classes,
    bool zero_init_residual,
    int64_t groups,
    int64_t width_per_group)
    : groups(groups),
      base_width(width_per_group),
      inplanes(64),
      conv1(
          torch::nn::Conv1dOptions(1, 64, 7).stride(2).padding(3).bias(false)),
      bn1(64),
      layer1(_make_layer(64, layers[0])),
      layer2(_make_layer(128, layers[1], 2)),
      layer3(_make_layer(256, layers[2], 2)),
      layer4(_make_layer(512, layers[3], 2)),
      fc(512 * Block::expansion, num_classes)
{
    register_module("conv1", conv1);
    register_module("bn1", bn1);
    register_module("fc", fc);

    register_module("layer1", layer1);
    register_module("layer2", layer2);
    register_module("layer3", layer3);
    register_module("layer4", layer4);

    for (auto &module : modules(/*include_self=*/false))
    {
        if (auto M = dynamic_cast<torch::nn::Conv1dImpl *>(module.get()))
            torch::nn::init::kaiming_normal_(
                M->weight,
                /*a=*/0,
                torch::kFanOut,
                torch::kReLU);
        else if (auto M = dynamic_cast<torch::nn::BatchNorm1dImpl *>(module.get()))
        {
            torch::nn::init::constant_(M->weight, 1);
            torch::nn::init::constant_(M->bias, 0);
        }
    }

    // Zero-initialize the last BN in each residual branch, so that the residual
    // branch starts with zeros, and each residual block behaves like an
    // identity. This improves the model by 0.2~0.3% according to
    // https://arxiv.org/abs/1706.02677
    if (zero_init_residual)
        for (auto &module : modules(/*include_self=*/false))
        {
            if (auto *M = dynamic_cast<_resnet_1dimpl::Bottleneck *>(module.get()))
                torch::nn::init::constant_(M->bn3->weight, 0);
            else if (auto *M = dynamic_cast<_resnet_1dimpl::BasicBlock *>(module.get()))
                torch::nn::init::constant_(M->bn2->weight, 0);
        }
}

template <typename Block>
torch::Tensor ResNet_1dImpl<Block>::forward(torch::Tensor x)
{
    x = conv1->forward(x);
    x = bn1->forward(x).relu_();
    x = torch::max_pool1d(x, 3, 2, 1);

    x = layer1->forward(x);
    x = layer2->forward(x);
    x = layer3->forward(x);
    x = layer4->forward(x);

    x = torch::adaptive_avg_pool1d(x, {1});
    x = x.reshape({x.size(0), -1});
    x = fc->forward(x);

    return x;
}

struct ResNet18_1dImpl : ResNet_1dImpl<_resnet_1dimpl::BasicBlock>
{
    ResNet18_1dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet34_1dImpl : ResNet_1dImpl<_resnet_1dimpl::BasicBlock>
{
    ResNet34_1dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet50_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    ResNet50_1dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet101_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    ResNet101_1dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNet152_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    ResNet152_1dImpl(int64_t num_classes = 1000, bool zero_init_residual = false);
};

struct ResNext50_32x4d_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    ResNext50_32x4d_1dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct ResNext101_32x8d_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    ResNext101_32x8d_1dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct WideResNet50_2_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    WideResNet50_2_1dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

struct WideResNet101_2_1dImpl : ResNet_1dImpl<_resnet_1dimpl::Bottleneck>
{
    WideResNet101_2_1dImpl(
        int64_t num_classes = 1000,
        bool zero_init_residual = false);
};

template <typename Block>
struct ResNet_1d : torch::nn::ModuleHolder<ResNet_1dImpl<Block>>
{
    using torch::nn::ModuleHolder<ResNet_1dImpl<Block>>::ModuleHolder;
};

TORCH_MODULE(ResNet18_2d);
TORCH_MODULE(ResNet34_2d);
TORCH_MODULE(ResNet50_2d);
TORCH_MODULE(ResNet101_2d);
TORCH_MODULE(ResNet152_2d);
TORCH_MODULE(ResNext50_32x4d_2d);
TORCH_MODULE(ResNext101_32x8d_2d);
TORCH_MODULE(WideResNet50_2_2d);
TORCH_MODULE(WideResNet101_2_2d);

TORCH_MODULE(ResNet18_1d);
TORCH_MODULE(ResNet34_1d);
TORCH_MODULE(ResNet50_1d);
TORCH_MODULE(ResNet101_1d);
TORCH_MODULE(ResNet152_1d);
TORCH_MODULE(ResNext50_32x4d_1d);
TORCH_MODULE(ResNext101_32x8d_1d);
TORCH_MODULE(WideResNet50_2_1d);
TORCH_MODULE(WideResNet101_2_1d);

} // namespace ProjectVoice

#endif // RESNET_HPP
