#include "Dataset.hpp"
#include <iostream>
#include <sndfile.hh>
#include "Utils.hpp"

using Example = torch::data::Example<>;
namespace fs = std::filesystem;

using Data = std::vector<phoneme>;

std::tuple<Data, std::unordered_map<std::string, long>> getDataset(const fs::path &p)
{
    std::unordered_map<std::string, long> phonemeToId;
    Data data;
    for (const auto &f : fs::recursive_directory_iterator(p))
    {
        std::string filePath = f.path().string();
        if (filePath.rfind(".PHN") != std::string::npos && fs::directory_entry(fs::path(filePath.substr(0, filePath.size() - 4) + ".WAV")).exists())
        {
            std::string relPath = fs::relative(f.path(), p).replace_extension(".WAV");
            std::ifstream in(filePath);
            int start, end;
            std::string ph;
            long currentId = 0;

            while (!in.eof())
            {
                in >> start >> end >> ph;
                if (end - start < 5000)
                {
                    if (!phonemeToId.contains(ph))
                        phonemeToId.emplace(std::pair{ph, currentId++});
                    data.emplace_back(phoneme{relPath, start, end, ph, phonemeToId[ph]});
                }
            }
        }
    }
    return std::tuple<Data, std::unordered_map<std::string, long>>(data, phonemeToId);
}

ProjectVoiceDataset::ProjectVoiceDataset(const std::vector<phoneme> &d,
                                         const std::unordered_map<std::string, long> &idMap,
                                         const fs::path &p)
{
    basePath = p;
    data = d;
    phonemeToId = idMap;
}

torch::Tensor ProjectVoiceDataset::toOneHot(long id)
{
    std::vector<long> oneHot(phonemeToId.size(), 0);
    oneHot[id] = 1;
    auto t = torch::tensor(oneHot, torch::kFloat);
    // std::cout << t <<"\n";
    return t;
}

Example ProjectVoiceDataset::get(size_t index)
{
    long id = data[index].id;
    auto samples = calculateAvgMfcc(basePath / data[index].relPath, data[index]);
    // std::vector<double> samples(5000, 0);

    // SndfileHandle sf(basePath / data[index].relPath);
    // sf.seek(data[index].start, 0);
    // sf.read(samples.data(), data[index].end - data[index].start);

    return {torch::tensor(samples, torch::kFloat), torch::tensor(id)};
}

torch::optional<size_t> ProjectVoiceDataset::size() const
{
    return data.size();
}
