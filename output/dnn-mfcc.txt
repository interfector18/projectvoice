Running on: CUDA
171568
73529
Train Epoch: 1, 171568 / 171568, Loss: 7.996330e-04, Acc: 1.2615e-01
Test Loss: 7.629062e-04, Acc: 1.668729e-01
Train Epoch: 2, 171568 / 171568, Loss: 7.341821e-04, Acc: 1.8505e-01
Test Loss: 7.085145e-04, Acc: 1.871234e-01
Train Epoch: 3, 171568 / 171568, Loss: 6.854988e-04, Acc: 1.9289e-01
Test Loss: 6.659874e-04, Acc: 1.907547e-01
Train Epoch: 4, 171568 / 171568, Loss: 6.468307e-04, Acc: 1.9589e-01
Test Loss: 6.318691e-04, Acc: 1.929715e-01
Train Epoch: 5, 171568 / 171568, Loss: 6.162848e-04, Acc: 1.9885e-01
Test Loss: 6.054050e-04, Acc: 1.966571e-01
Train Epoch: 6, 171568 / 171568, Loss: 5.931445e-04, Acc: 2.0227e-01
Test Loss: 5.857513e-04, Acc: 1.999347e-01
Train Epoch: 7, 171568 / 171568, Loss: 5.762498e-04, Acc: 2.0275e-01
Test Loss: 5.715728e-04, Acc: 1.968475e-01
Train Epoch: 8, 171568 / 171568, Loss: 5.640545e-04, Acc: 2.0744e-01
Test Loss: 5.612163e-04, Acc: 2.068980e-01
Train Epoch: 9, 171568 / 171568, Loss: 5.549662e-04, Acc: 2.1132e-01
Test Loss: 5.533047e-04, Acc: 2.088972e-01
Train Epoch: 10, 171568 / 171568, Loss: 5.478949e-04, Acc: 2.1263e-01
Test Loss: 5.470729e-04, Acc: 2.102028e-01