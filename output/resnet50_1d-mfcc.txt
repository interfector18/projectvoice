Running on: CUDA
171568
73529
Train Epoch: 1, 171568 / 171568, Loss: 3.822818e-03, Acc: 1.2041e-01
Test Loss: 3.103265e-03, Acc: 1.238695e-01
Train Epoch: 2, 171568 / 171568, Loss: 2.891257e-03, Acc: 1.4581e-01
Test Loss: 2.874984e-03, Acc: 1.423656e-01
Train Epoch: 3, 171568 / 171568, Loss: 2.772760e-03, Acc: 1.6010e-01
Test Loss: 2.771571e-03, Acc: 1.544289e-01
Train Epoch: 4, 171568 / 171568, Loss: 2.705065e-03, Acc: 1.7339e-01
Test Loss: 2.717715e-03, Acc: 1.657849e-01
Train Epoch: 5, 171568 / 171568, Loss: 2.656720e-03, Acc: 1.8308e-01
Test Loss: 2.663663e-03, Acc: 1.748426e-01
Train Epoch: 6, 171568 / 171568, Loss: 2.609614e-03, Acc: 1.9230e-01
Test Loss: 2.618103e-03, Acc: 1.820098e-01
Train Epoch: 7, 171568 / 171568, Loss: 2.577430e-03, Acc: 1.9902e-01
Test Loss: 2.591743e-03, Acc: 1.989555e-01
Train Epoch: 8, 171568 / 171568, Loss: 2.551731e-03, Acc: 2.0411e-01
Test Loss: 2.560475e-03, Acc: 2.062588e-01
Train Epoch: 9, 171568 / 171568, Loss: 2.527699e-03, Acc: 2.0877e-01
Test Loss: 2.541415e-03, Acc: 2.095636e-01
Train Epoch: 10, 171568 / 171568, Loss: 2.508815e-03, Acc: 2.1265e-01
Test Loss: 2.529637e-03, Acc: 1.990371e-01
Train Epoch: 11, 171568 / 171568, Loss: 2.491297e-03, Acc: 2.1670e-01
Test Loss: 2.505954e-03, Acc: 2.152484e-01
Train Epoch: 12, 171568 / 171568, Loss: 2.475494e-03, Acc: 2.1964e-01
Test Loss: 2.493008e-03, Acc: 2.176692e-01
Train Epoch: 13, 171568 / 171568, Loss: 2.460404e-03, Acc: 2.2323e-01
Test Loss: 2.480588e-03, Acc: 2.214228e-01
Train Epoch: 14, 171568 / 171568, Loss: 2.446933e-03, Acc: 2.2608e-01
Test Loss: 2.472348e-03, Acc: 2.234628e-01
Train Epoch: 15, 171568 / 171568, Loss: 2.435917e-03, Acc: 2.2806e-01
Test Loss: 2.464991e-03, Acc: 2.254485e-01
Train Epoch: 16, 171568 / 171568, Loss: 2.426473e-03, Acc: 2.3049e-01
Test Loss: 2.454857e-03, Acc: 2.273933e-01
Train Epoch: 17, 171568 / 171568, Loss: 2.414778e-03, Acc: 2.3347e-01
Test Loss: 2.448714e-03, Acc: 2.289573e-01
Train Epoch: 18, 171568 / 171568, Loss: 2.404470e-03, Acc: 2.3678e-01
Test Loss: 2.441390e-03, Acc: 2.308749e-01
Train Epoch: 19, 171568 / 171568, Loss: 2.395157e-03, Acc: 2.3810e-01
Test Loss: 2.435968e-03, Acc: 2.326565e-01
Train Epoch: 20, 171568 / 171568, Loss: 2.383968e-03, Acc: 2.4157e-01
Test Loss: 2.428510e-03, Acc: 2.324661e-01